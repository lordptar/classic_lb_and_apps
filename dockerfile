FROM ubuntu:trusty                                                                                                                  

# install dependencies 
RUN apt-get update && apt-get install -y npm nodejs-legacy

ADD devops-test-webapp app

WORKDIR /app

EXPOSE 3000
 
CMD "npm" "start"
